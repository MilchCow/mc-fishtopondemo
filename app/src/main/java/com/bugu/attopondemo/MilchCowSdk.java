package com.bugu.attopondemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.anythink.banner.api.ATBannerListener;
import com.anythink.banner.api.ATBannerView;
import com.anythink.core.api.ATAdInfo;
import com.anythink.core.api.ATSDK;
import com.anythink.core.api.AdError;
import com.anythink.interstitial.api.ATInterstitial;
import com.anythink.interstitial.api.ATInterstitialListener;
import com.anythink.nativead.api.ATNative;
import com.anythink.nativead.api.ATNativeNetworkListener;
import com.anythink.rewardvideo.api.ATRewardVideoAd;
import com.anythink.rewardvideo.api.ATRewardVideoListener;
import com.fish.anythink.nativead.api.FloatProviderHelper;
import com.fish.anythink.splashad.api.IATFloatContainerProvider;
import com.fish.support.view.DragHelper;
import com.fish.support.view.DragView;
import com.mobi.fish.polymeric.TOSDK;
import com.mobi.fish.polymeric.TOSDKInitListener;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Heyy
 * @date 2020/11/23
 */
public class MilchCowSdk {
    private static Context sContext;
    private static Activity sActivity;

    private static String sTopOnAppId = "a5f9292bb549a5";
    private static String sTopOnAppKey = "39b8145fb73e6c2b94098e95bad31fe9";
    private static String sTopOnBannerId = "b5f9299f096c2b";
    private static String sTopOnIntersId = "b5f9292da07ac3";
    private static String sTopOnRewardId = "b5f9292c794c03";
    private static String sTopOnSplashId = "b5f92935c68eac";
    private static String sMobiaiFloatId = "b5fb603aabf160";


    private static ATBannerView sTopOnBannerAd;
    private static ATInterstitial sTopOnIntersAd;
    private static ATRewardVideoAd sTopOnRewardAd;
    private static boolean isInitOnce = false;

    // 测试模式时打开调试日志
    private static boolean isDebugMode = true;

    private static boolean isInitBanner = false;
    private static boolean isTopOnInitSuccess = false;

    private static FrameLayout sFrameLayout;

    private static int INTERVAL_TIME = 12000;

    private static String ERRORCODE_ADS_IS_LOADING = "2005";


    public static void init(final Context context, final IMilchCowSdkListener listener) {
        sContext = context;
        if (sContext instanceof Activity) {
            sActivity = (Activity) sContext;
        }

        if (!isInitOnce) {
            isInitOnce = true;
            // 检查获取手机权限
            PermissionChecker.requestReadPhoneState(sActivity);

            // 初始化TopOnSDK
            if (sActivity != null) {
                sActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initTopOnSdk(listener);
                    }
                });
            } else {
                errorLog("TopOnSDK init must in main thread!");
            }

        } else {
            Log.w("milchcow_sdk", "MilchCowSdk only needs to be initialized once.");
        }
    }

    private static void initTopOnSdk(final IMilchCowSdkListener listener) {
        infoLog("initTopOnSdk TopOnAppId:" + sTopOnAppId + ", TopOnAppKey:" + sTopOnAppKey);
        if (isDebugMode) {
            ATSDK.setNetworkLogDebug(true);// 打开日志（Tag:"anythink"）,正式关闭debugLog("TopOnSDK InitSuccess");
            ATSDK.integrationChecking(sContext); // 检查各平台广告是否接入,正式关闭
//            TOSDK.DEBUG = true;// Mobiai平台Log（Tag:"custom_log"）
        }
        TOSDK.init(sContext, sTopOnAppId, sTopOnAppKey, new TOSDKInitListener() {
            @Override
            public void onInitSuccess() {
                debugLog("TopOnSDK InitSuccess");
                if (!isTopOnInitSuccess) {
                    isTopOnInitSuccess = true;
                    if (listener != null) {
                        listener.onSuccess();
                    }

                    loadTopOnAds();
                }

            }

            @Override
            public void onInitFailed(int code, String msg) {
                errorLog("TopOnSDK InitFailed errorCode:" + code + ", msg:" + msg);
                isTopOnInitSuccess = false;
                if (listener != null) {
                    listener.onFailure(code, msg);
                }

            }
        });

    }

    private static void loadTopOnAds() {
        // 展示浮标广告
        showMobiaiFloatAds();
//
//        // 展示开屏广告
        showSplashAds();
//
//        // 展示横幅广告
        loadBannerAds("", 0, 0);
    }

    private static void showSplashAds() {
        debugLog("showSplashAds.");
        if (sActivity != null && sContext != null) {
            sActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(sContext, SplashActivity.class);
                    intent.putExtra("splash_ad_id", sTopOnSplashId);
                    sContext.startActivity(intent);
                }
            });
        } else {
            Log.e("milchcow_sdk", "MilchCowSdk showSplashAds sActivity is null.");
        }
    }

    public static void loadBannerAds(final String buguAdsId, final int width, final int height) {
        debugLog("loadBannerAds TopOnBannerId:" + sTopOnBannerId);
        if (isInitBanner) {
            warnLog("Banner only load once.");
            return;
        }
        isInitBanner = true;
        loadBannerOnSafeThread(buguAdsId);
    }

    private static void loadBannerOnSafeThread(final String buguAdsId) {
        if (sActivity != null) {
            sActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FrameLayout frameLayout = initBannerContainer();
                    frameLayout.setVisibility(View.VISIBLE);
                    if (sTopOnBannerAd == null) {
                        sTopOnBannerAd = new ATBannerView(sActivity);
                        setTopOnBannerListener(sTopOnBannerAd, buguAdsId);
                        frameLayout.addView(sTopOnBannerAd, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, dip2px(60)));
                    }
                    sTopOnBannerAd.setUnitId(sTopOnBannerId);
                    sTopOnBannerAd.loadAd();
                }
            });
        } else {
            errorLog("loadBannerAds sActivity is null!");
        }
    }

    private static void setTopOnBannerListener(ATBannerView bannerView, final String buguAdsId) {
        bannerView.setBannerAdListener(new ATBannerListener() {
            @Override
            public void onBannerLoaded() {
                debugLog("TopOnAds BannerLoaded");

            }

            @Override
            public void onBannerFailed(AdError adError) {
                errorLog("TopOnAds BannerFailed:" + adError.getFullErrorInfo());

                if (!ERRORCODE_ADS_IS_LOADING.equals(adError.getCode())) {
                    // 如果广告不是正在加载中的错误就延时10s后再加载
                    Timer timer = new Timer();
                    TimerTask timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            isInitBanner = false;
                            loadBannerAds(buguAdsId, 0, 0);
                        }
                    };
                    timer.schedule(timerTask, INTERVAL_TIME);
                }
            }

            @Override
            public void onBannerClicked(ATAdInfo atAdInfo) {
                debugLog("TopOnAds BannerClicked");

            }

            @Override
            public void onBannerShow(ATAdInfo atAdInfo) {
                debugLog("TopOnAds BannerShow");

            }

            @Override
            public void onBannerClose(ATAdInfo atAdInfo) {
                debugLog("TopOnAds BannerClose");

                isInitBanner = false;
                loadBannerAds(buguAdsId, 0, 0);
            }

            @Override
            public void onBannerAutoRefreshed(ATAdInfo atAdInfo) {
                verboseLog("TopOnAds BannerClose");
            }

            @Override
            public void onBannerAutoRefreshFail(AdError adError) {
                errorLog("TopOnAds BannerAutoRefreshFail:" + adError.getFullErrorInfo());

                Timer timer = new Timer();
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        isInitBanner = false;
                        loadBannerAds(buguAdsId, 0, 0);
                    }
                };
                timer.schedule(timerTask, INTERVAL_TIME);
            }
        });
    }

    private static synchronized FrameLayout initBannerContainer() {
        if (sFrameLayout == null) {
            if (sTopOnBannerAd != null) {
                sTopOnBannerAd.destroy();
                sTopOnBannerAd = null;
            }
            ViewGroup decorView = (ViewGroup) sActivity.getWindow().getDecorView();
            sFrameLayout = new FrameLayout(sActivity);
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.BOTTOM;
            decorView.addView(sFrameLayout, lp);
        }
        return sFrameLayout;
    }

    public static void showBannerAds(Activity activity, final String buguAdsId, String position) {
        debugLog("TopOnAds showBannerAds");
        if (sFrameLayout != null && activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sFrameLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public static void hideBanner() {
        debugLog("TopOnAds hideBanner");
        if (sFrameLayout != null && sActivity != null) {
            sActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sFrameLayout.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public static void loadFullScreenVideoAds(String buguAdsId) {
        debugLog("TopOnAds loadFullScreenVideoAds");

        if (sTopOnIntersAd == null) {
            sTopOnIntersAd = new ATInterstitial(sContext, sTopOnIntersId);
            setTopOnIntersListener(buguAdsId);
        }
        sTopOnIntersAd.load();
    }

    private static void setTopOnIntersListener(final String buguAdsId) {
        if (sTopOnIntersAd == null) {
            errorLog("setTopOnIntersListener sInterstitialAd is null!");
            return;
        }
        sTopOnIntersAd.setAdListener(new ATInterstitialListener() {
            @Override
            public void onInterstitialAdLoaded() {
                debugLog("TopOnAds InterstitialAdLoaded");

            }

            @Override
            public void onInterstitialAdLoadFail(AdError adError) {
                errorLog("TopOnAds InterstitialAdLoadFail:" + adError.getFullErrorInfo());
            }

            @Override
            public void onInterstitialAdClicked(ATAdInfo entity) {
                debugLog("TopOnAds InterstitialClicked");

            }

            @Override
            public void onInterstitialAdShow(ATAdInfo entity) {
                debugLog("TopOnAds InterstitialShow");

            }

            @Override
            public void onInterstitialAdClose(ATAdInfo entity) {
                //建议在此回调中调用load进行广告的加载，方便下一次广告的展示
                debugLog("TopOnAds InterstitialClose");

                loadFullScreenVideoAds(buguAdsId);

            }

            //v5.5.3新增ATAdInfo参数
            @Override
            public void onInterstitialAdVideoStart(ATAdInfo entity) {
            }

            //v5.5.3新增ATAdInfo参数
            @Override
            public void onInterstitialAdVideoEnd(ATAdInfo entity) {
            }

            @Override
            public void onInterstitialAdVideoError(AdError adError) {
                errorLog("TopOnAds onInterstitialAdVideoErrorer:" + adError.getFullErrorInfo());

                Timer timer = new Timer();
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        loadFullScreenVideoAds(buguAdsId);
                    }
                };
                timer.schedule(timerTask, INTERVAL_TIME);
            }
        });
    }

    public static void showFullScreenVideoAds(String buguAdsId, final Activity activity) {
        debugLog("TopOnAds showFullScreenVideoAds");

        if (activity != null && sTopOnIntersAd != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sTopOnIntersAd.show(activity);
                }
            });
        } else {
            errorLog("TopOnAds showFullScreenVideoAds Failed.");
        }

    }

    public static void loadRewardVideoAds(String buguAdsId) {
        debugLog("TopOnAds loadRewardVideoAds");

        if (sTopOnRewardAd == null) {
            sTopOnRewardAd = new ATRewardVideoAd(sActivity, sTopOnRewardId);
            setTopOnRewardListener(buguAdsId);
        }
        sTopOnRewardAd.load();

    }

    private static void setTopOnRewardListener(final String buguAdsId) {
        if (sTopOnRewardAd == null) {
            errorLog("setTopOnRewardListener sTopOnRewardAd is null!");
            return;
        }
        sTopOnRewardAd.setAdListener(new ATRewardVideoListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                debugLog("TopOnAds onRewardedVideoAdLoaded");

            }

            @Override
            public void onRewardedVideoAdFailed(AdError adError) {
                errorLog("TopOnAds onRewardedVideoAdFailed error:" + adError.getFullErrorInfo());

                if (!ERRORCODE_ADS_IS_LOADING.equals(adError.getCode())) {
                    // 如果广告不是正在加载中的错误就延时10s后再加载
                    Timer timer = new Timer();
                    TimerTask timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            loadRewardVideoAds(buguAdsId);
                        }
                    };
                    timer.schedule(timerTask, INTERVAL_TIME);
                }

            }

            @Override
            public void onRewardedVideoAdPlayStart(ATAdInfo entity) {

                verboseLog("TopOnAds onRewardedVideoAdPlayStart:\n" + entity.toString());

            }

            @Override
            public void onRewardedVideoAdPlayEnd(ATAdInfo entity) {
                verboseLog("TopOnAds onRewardedVideoAdPlayEnd:\n" + entity.toString());
            }

            @Override
            public void onRewardedVideoAdPlayFailed(AdError adError, ATAdInfo entity) {
                errorLog("TopOnAds onRewardedVideoAdPlayFailed error:" + adError.getFullErrorInfo());
                Timer timer = new Timer();
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        loadRewardVideoAds(buguAdsId);
                    }
                };
                timer.schedule(timerTask, INTERVAL_TIME);
            }

            @Override
            public void onRewardedVideoAdClosed(ATAdInfo entity) {
                debugLog("TopOnAds onRewardedVideoAdClosed");

                loadRewardVideoAds(buguAdsId);
            }

            @Override
            public void onRewardedVideoAdPlayClicked(ATAdInfo entity) {
                debugLog("TopOnAds onRewardedVideoAdPlayClicked:");

            }

            @Override
            public void onReward(ATAdInfo entity) {
                debugLog("TopOnAds onReward:\n" + entity.toString());

            }
        });
    }

    public static void showRewardVideoAds(final String buguAdsId, final Activity activity) {
        debugLog("TopOnAds showRewardVideoAds TopOnRewardId:" + sTopOnRewardId);

        if (activity != null && sTopOnRewardAd != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sTopOnRewardAd.show(activity, sTopOnRewardId);
                }
            });
        } else {
            errorLog("TopOnAds showRewardVideoAds Failed.");
        }
    }

    public static void showMobiaiFloatAds() {
        debugLog("showMobiaiFloatAds.");
        if (sActivity != null) {
            sActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showMobiaiFloatAdsOnSafeThread();
                }
            });
        } else {
            Log.e("milchcow_sdk", "MilchCowSdk showMobiaiFloatAds sActivity is null!");
        }

    }

    private static void showMobiaiFloatAdsOnSafeThread() {
        if (sActivity != null) {
            debugLog("showMobiaiFloatAds22222.");
            FloatProviderHelper.getInstance().setProvider(new ATFloatContainerProvider());
            ATNative atNative = new ATNative(sActivity, sMobiaiFloatId, new ATNativeNetworkListener() {
                @Override
                public void onNativeAdLoaded() {
                    debugLog("showMobiaiFloatAdsOnSafeThread onNativeAdLoaded.");
                }

                @Override
                public void onNativeAdLoadFail(AdError adError) {
                    Log.e("milchcow_sdk", "showMobiaiFloatAdsOnSafeThread onNoAdError:" + adError.getFullErrorInfo());
                    Timer timer = new Timer();
                    TimerTask timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            showMobiaiFloatAds();
                        }
                    };
                    timer.schedule(timerTask, INTERVAL_TIME);
                }
            });
            atNative.makeAdRequest();
        }

    }

    public static void hideMobiaiFloatAds() {
//        if (sATFloatAdContainer != null && sActivity != null) {
//            sActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    sATFloatAdContainer.setVisibility(View.INVISIBLE);
//                }
//            });
//        }
    }

    private static class ATFloatContainerProvider implements IATFloatContainerProvider {

        @Override
        public ViewGroup responseParent() {
            debugLog("responseParent----------------------");
            if (sActivity != null) {
                DragView floatContainer = DragHelper.getFloatContainer(sActivity);
                Log.e("milchcow_sdk", "DragView is null?" + (floatContainer == null ? true : false));
            }

            return DragHelper.getFloatContainer(sActivity);
        }
    }

    private static int dip2px(float dipValue) {
        float scale = sContext.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    private static void verboseLog(String message) {
        if (isDebugMode) {
            Log.v("milchcow_sdk", "MilchCowSdk " + message);
        }
    }

    public static void debugLog(String message) {
        if (isDebugMode) {
            Log.d("milchcow_sdk", "MilchCowSdk " + message);
        }
    }

    private static void infoLog(String message) {
        if (isDebugMode) {
            Log.i("milchcow_sdk", "MilchCowSdk " + message);
        }
    }

    private static void warnLog(String message) {
        if (isDebugMode) {
            Log.w("milchcow_sdk", "MilchCowSdk " + message);
        }
    }

    public static void errorLog(String message) {
        if (isDebugMode) {
            Log.e("milchcow_sdk", "MilchCowSdk " + message);
        }
    }
}
