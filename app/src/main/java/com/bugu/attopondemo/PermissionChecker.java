package com.bugu.attopondemo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

public class PermissionChecker {
    public static final int READ_PHONE_STATE = 90001;

    public static boolean requestReadPhoneState(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity != null) {
            int hasWriteContactsPermission = activity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                activity.requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        READ_PHONE_STATE);
                return false;
            }
        }
        return true;
    }
}
