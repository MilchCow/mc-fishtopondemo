package com.bugu.attopondemo;

/**
 * @author Heyy
 * @date 2020/11/23
 */
public interface IMilchCowSdkListener {
    void onFailure(int errCode, String errMessage);

    void onSuccess();
}
