package com.bugu.attopondemo;

import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.anythink.core.api.ATAdInfo;
import com.anythink.core.api.ATMediationRequestInfo;
import com.anythink.core.api.AdError;
import com.anythink.splashad.api.ATSplashAd;
import com.anythink.splashad.api.ATSplashAdListener;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;


/**
 * @author Heyy
 * @date 2020/11/9
 */
public class SplashActivity extends FragmentActivity implements ATSplashAdListener {

    ATSplashAd splashAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String splashAdId = getIntent().getStringExtra("splash_ad_id");
        FrameLayout container = findViewById(R.id.splash_ad_container);
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        /**You should set size to the layout param.**/
        layoutParams.width = getResources().getDisplayMetrics().widthPixels;
        layoutParams.height = getResources().getDisplayMetrics().heightPixels;
        ATMediationRequestInfo atMediationRequestInfo = null;
//        atMediationRequestInfo = new MintegralATRequestInfo("100947", "ef13ef712aeb0f6eb3d698c4c08add96", "210169", "276803");
//        atMediationRequestInfo.setAdSourceId("71606");
        splashAd = new ATSplashAd(this, container, splashAdId, atMediationRequestInfo, this);
    }

    @Override
    public void onAdLoaded() {
        MilchCowSdk.debugLog("SplashAdShowActivity onAdLoaded.");
    }

    @Override
    public void onNoAdError(AdError adError) {
        Log.e("bugu_sdk", "SplashAdShowActivity onNoAdError:\n" + adError.getFullErrorInfo());
        cloasSplashAds();
    }

    @Override
    public void onAdShow(ATAdInfo atAdInfo) {
        MilchCowSdk.debugLog("SplashAdShowActivity onAdShow:\n" + atAdInfo.toString());
    }

    @Override
    public void onAdClick(ATAdInfo atAdInfo) {
        MilchCowSdk.debugLog("SplashAdShowActivity onAdClick:\n" + atAdInfo.toString());
    }

    @Override
    public void onAdDismiss(ATAdInfo atAdInfo) {
        MilchCowSdk.debugLog("SplashAdShowActivity onAdDismiss:\n" + atAdInfo.toString());
        cloasSplashAds();
    }

    @Override
    public void onAdTick(long l) {

    }

    private void cloasSplashAds() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (splashAd != null) {
            splashAd.onDestory();
        }
    }
}
