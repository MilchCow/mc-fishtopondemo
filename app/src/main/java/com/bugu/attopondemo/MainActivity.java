package com.bugu.attopondemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void initMilchCowSDK(View view) {
        MilchCowSdk.init(this, new IMilchCowSdkListener() {
            @Override
            public void onFailure(int errCode, String errMessage) {
                Log.v("bugu_MainActivity", "initBuguSDK onFailure");
            }

            @Override
            public void onSuccess() {
                Log.v("bugu_MainActivity", "initBuguSDK onSuccess");
            }
        });
    }

    public void loadRewardVideoAds(View view) {
        MilchCowSdk.loadRewardVideoAds("333");
    }

    public void showRewardVideoAds(View view) {
        MilchCowSdk.showRewardVideoAds("333", this);
    }

    public void loadFullScreenVideoAds(View view) {
        MilchCowSdk.loadFullScreenVideoAds("222");
    }

    public void showFullScreenVideoAds(View view) {
        MilchCowSdk.showFullScreenVideoAds("222", this);
    }

    public void loadBannerAds(View view) {
        MilchCowSdk.loadBannerAds("111", 0, 0);
    }

    public void showBannerAds(View view) {
        MilchCowSdk.showBannerAds(this, "111", "");
    }

    public void hideBannerAds(View view) {
        MilchCowSdk.hideBanner();
    }

    public void showMobiaiFloatAds(View view) {
        MilchCowSdk.showMobiaiFloatAds();
    }

    public void hideMobiaiFloatAds(View view) {
        MilchCowSdk.hideMobiaiFloatAds();
    }
}
